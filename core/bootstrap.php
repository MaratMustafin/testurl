<?php

$config = require 'config.php';
$variable = require 'core/Router.php';
require 'core/database/Connection.php';
require 'core/database/QueryBuilder.php';
//var_dump($variable);
// $pdo = Connection::make();

return new QueryBuilder(Connection::make(
    $config['database']
));
