<?php 

// class Contractor 
// {
//     protected $member1;
//     protected $member2;
//     public function __construct($member1,$member2,$member3) 
//     {
//         $this->member1 = $member1;
//         $this->member2 = $member2;
//     }
//     public function performWork()
//     {

//     }
// }

class QueryBuilder 
{
    protected $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }
    public function selectAll($table)
    {
        $statement = $this->pdo->prepare("select * from {$table}");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_CLASS);
    }
}